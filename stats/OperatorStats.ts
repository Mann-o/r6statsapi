export interface OperatorStats {
    username: string;
    platform: string;
    ubisoft_id: string;
    uplay_id: string;
    avatar_url_146: string;
    avatar_url_256: string;
    last_updated: string;
    operators: Operator[];
}

interface Operator {
    name: string;
    ctu: string;
    role: string;
    kills: number;
    deaths: number;
    kd: number;
    wins: number;
    losses: number;
    wl: number;
    headshots: number;
    dbnos: number;
    melee_kills: number;
    experience: number;
    playtime: number;
    abilities?: Ability[];
}

interface Ability {
    ability: string;
    value: number;
}
