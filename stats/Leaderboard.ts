export interface LeaderboardEntry {
    username: string;
    platform: string;
    ubisoft_id: string;
    uplay_id: string;
    avatar_url_146: string;
    avatar_url_256: string;
    stats: Stats;
    score: number;
    position: number;
}

interface Stats {
    level: number;
    kd: number;
    wl?: number;
}
