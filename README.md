# R6StatsAPI

Unofficial R6Stats API client library for Node.js.

## Example

```js
const R6StatsAPI = require('r6statsapi').default;

const API = new R6StatsAPI('your-token-here');

API.getGenericStats('Kev.-', 'pc', 'all').then(userStats => {
    console.log(userStats.stats.general.barricades_deployed); // 1089
});

// or with async/await

let userStats = await API.getGenericStats('Kev.-', 'pc', 'all');
console.log(userStats.stats.general.barricades_deployed); // 1089
```

## Available Methods

All methods return a `Promise` with the stats.

### getLeaderboard(platform, region)

Get the R6Stats leaderboard.

-   `platform`: The platform of the game
-   `region`: The region of the leaderboard, default is `all`

### getGenericStats(username, platform)

Get general statistics for a player.

-   `username`: The username of the player
-   `platform`: The platform the player plays the game on

### getSeasonalStats(username, platform)

Get seasonal statistics for a player.

-   `username`: The username of the player
-   `platform`: The platform the player plays the game on

### getOperatorStats(username, platform)

Get operator statistics for a player.

-   `username`: The username of the player
-   `platform`: The platform the player plays the game on

### getWeaponCategoryStats(username, platform)

Get weapon statistics by category for a player.

-   `username`: The username of the player
-   `platform`: The platform the player plays the game on

### getWeaponStats(username, platform)

Get weapon statistics for a player.

-   `username`: The username of the player
-   `platform`: The platform the player plays the game on

### \_getStats(username, platform, type)

Get game stats about a specific player.

-   `username` The username of the player
-   `platform` The platform the player plays the game on
-   `type` The type of stats the request
