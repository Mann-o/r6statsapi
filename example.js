import R6StatsAPI from './r6stats';

const API = new R6StatsAPI('your-api-key');

(async () => {
    let result = await API.getGenericStats('Kev.-', 'pc', 'all');

    console.log(result.stats.general.barricades_deployed); // 1089
})();
